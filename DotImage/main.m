//
//  main.m
//  DotImage
//
//  Created by Abizer Nasir on 07/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
