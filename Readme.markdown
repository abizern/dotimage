## DotImage

Creates a clipping rectangle at the touch point and clears a part of an image.

This example is provided as a solution to a Stack Overflow Question:
[14755840](http://stackoverflow.com/questions/14755840/cgcontextclearrect-with-circle)
