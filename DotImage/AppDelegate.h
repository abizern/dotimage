//
//  AppDelegate.h
//  DotImage
//
//  Created by Abizer Nasir on 07/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
