//
//  ViewController.h
//  DotImage
//
//  Created by Abizer Nasir on 07/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)clearCircle:(id)sender;

@end
