//
//  ViewController.m
//  DotImage
//
//  Created by Abizer Nasir on 07/02/2013.
//  Copyright (c) 2013 Jungle Candy Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
    self.imageView.image = [self croppedImage];
}

- (IBAction)clearCircle:(UITapGestureRecognizer *)sender {
    UIImageView *imageView = self.imageView;
    UIImage *currentImage = imageView.image;
    CGSize imageViewSize = imageView.bounds.size;

    CGFloat rectWidth = 40.0f;
    CGFloat rectHeight = 40.0f;
    
    CGPoint touchPoint = [sender locationInView:imageView];

    UIGraphicsBeginImageContextWithOptions(imageViewSize, YES, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();

    [currentImage drawAtPoint:CGPointZero];

    CGRect clippingEllipseRect = CGRectMake(touchPoint.x - rectWidth / 2, touchPoint.y - rectHeight / 2, rectWidth, rectHeight);
    CGContextAddEllipseInRect(ctx, clippingEllipseRect);

    CGContextClip(ctx);

    CGContextClearRect(ctx, clippingEllipseRect);
    
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
}

#pragma mark - Private helper methods

- (UIImage *)croppedImage {
    // Convenience function which just crops the image to fit into the Image view
    // Stick it into the top left corner and just draw as much as fits regardless of
    // whether this is on a 3.5 or 4 inch device.
    NSURL *imageURL = [[NSBundle mainBundle] URLForResource:@"Bikes" withExtension:@"jpg"];
    UIImage *rawImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];

    CGSize imageViewSize = self.imageView.bounds.size;

    UIGraphicsBeginImageContextWithOptions(imageViewSize, YES, 0.0f);
    [rawImage drawAtPoint:CGPointZero];

    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return croppedImage;
}


@end
